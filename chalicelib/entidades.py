from chalice import Chalice

list_entidades ={"AA":
    {
      "codigo": "AA",
      "nombre": "Presidencia de la República",
      "mision": "",
      "sigla": "",
      "created_at": "null",
      "updated_at": "null"
    },"AB":
    {
      "codigo": "AB",
      "nombre": "Ministerio del Interior y Seguridad Pública",
      "mision": "Promover e impulsar políticas de desarrollo regional, provincial y local, evaluando su ejecución. Fiscalizar el cumplimiento de normas legales, reglamentarias y técnicas y de las instrucciones que se dicten para la administración civil del Estado, en materias atingentes a las áreas de desarrollo regional, modernización y reforma administrativa.",
      "sigla": "",
      "created_at": "null",
      "updated_at": "null"
    },"AC":
    {
      "codigo": "AC",
      "nombre": "Ministerio de Relaciones Exteriores",
      "mision": "Su misión es planificar, dirigir, coordinar, ejecutar y difundir la Política Exterior que formula la Presidencia de la República.",
      "sigla": "",
      "created_at": "null",
      "updated_at": "null"
    },"AD":
    {
      "codigo": "AD",
      "nombre": "Ministerio de Defensa Nacional",
      "mision": "Proteger a la población, preservar el territorio nacional y resguardar la capacidad del Estado para el ejercicio de su soberanía frente a amenazas externas contra estos elementos fundamentales del país, así como apoyar el logro de los objetivos nacionales.",
      "sigla": "",
      "created_at": "null",
      "updated_at": "null"
    },"AE":
    {
      "codigo": "AE",
      "nombre": "Ministerio de Hacienda",
      "mision": "Su misión es maximizar la tasa de crecimiento de la economía, logrando el mejor uso y rendimiento de los recursos productivos con que cuenta el país, para así alcanzar un crecimiento económico alto y estable que se traduzca en una mejor calidad de vida para todos los chilenos y chilenas, especialmente los sectores más postergados y vulnerables.",
      "sigla": "",
      "created_at": "null",
      "updated_at": "null"
    },"AF":
    {
      "codigo": "AF",
      "nombre": "Ministerio Secretaría General de la Presidencia",
      "mision": "facilitar y coordinar el desarrollo y cumplimiento de la agenda programática y legislativa del gobierno a través de las siguientes acciones: Asesorar al Presidente de la República y Ministros de Estado en materias políticas, jurídicas y administrativas y en las relaciones del gobierno con el Congreso Nacional, los partidos políticos y organizaciones sociales. Velar por el logro de una efectiva coordinación programática general de la gestión del gobierno. Participar en la elaboración de la agenda legislativa y hacer el seguimiento de la tramitación de los proyectos de ley. Efectuar estudios y análisis relevantes para la toma de decisiones.",
      "sigla": "MINSEGPRES",
      "created_at": "null",
      "updated_at": "null"
    },"AG":
    {
      "codigo": "AG",
      "nombre": "Ministerio Secretaría General de Gobierno",
      "mision": "Facilitar la comunicación entre el gobierno y la sociedad chilena, difundiendo las decisiones, iniciativas, mensajes centrales, actividades, beneficios y oportunidades emanados desde el Ejecutivo. Para ello, debe asimismo asesorar y coordinar en este ámbito a los ministerios y servicios, y establecer canales de vinculación con las personas y las organizaciones sociales, de manera tal que sus opiniones, expectativas e inquietudes sean recogidas a tiempo y en un contexto de plena transparencia.",
      "sigla": "SEGEGOB",
      "created_at": "null",
      "updated_at": "null"
    }, "AH":
    {
      "codigo": "AH",
      "nombre": "Ministerio de Economía, Fomento y Turismo",
      "mision": "Su misión es promover la modernización y competitividad de la estructura productiva del país, la iniciativa privada y la acción eficiente de los mercados, el desarrollo de la innovación y la consolidación de la inserción internacional de la economía del país. De esta forma espera lograr un crecimiento sostenido, sustentable y con equidad.",
      "sigla": "",
      "created_at": "null",
      "updated_at": "null"
    },"AI":
    {
      "codigo": "AI",
      "nombre": "Ministro de Desarrollo Social",
      "mision": "La misión del Ministerio de Planificación es promover el desarrollo del país con integración y protección social de las personas, articulando acciones con las autoridades políticas, órganos del Estado y sociedad civil, a nivel nacional, regional y local, mediante el análisis, diseño, coordinación, ejecución y evaluación de políticas sociales, planes y programas; la evaluación de las iniciativas de inversión pública; la provisión de información y análisis acerca de la realidad social y económica; y la elaboración de instrumentos y metodologías para la gestión y toma de decisiones de políticas públicas.",
      "sigla": "MIDEPLAN",
      "created_at": "null",
      "updated_at": "null"
    },"AJ":
    {
      "codigo": "AJ",
      "nombre": "Ministerio de Educación",
      "mision": "La misión del Ministerio de Educación es fomentar el desarrollo de la educación en todos sus niveles y promover el progreso integral de todas las personas, a través de un sistema educativo que asegure igualdad de oportunidades y aprendizaje de calidad para todos los niños/as, jóvenes y adultos durante su vida, con independencia de la edad y el sexo; otorgándoles una educación humanista, democrática, de excelencia y abierta al mundo en todos los niveles de enseñanza, cautelando el buen uso de los recursos públicos y contribuyendo activamente a la garantïza del derecho a la educación y a la libertad de enseñanza.",
      "sigla": "MINEDUC",
      "created_at": "null",
      "updated_at": "null"
    },"AK":
    {
      "codigo": "AK",
      "nombre": "Ministerio de Justicia",
      "mision": "Es la Secretaría de Estado encargada esencialmente de relacionar al Poder Ejecutivo con el Poder Judicial y de ejecutar las acciones que la ley y el Presidente de la República encomienden.",
      "sigla": "",
      "created_at": "null",
      "updated_at": "null"
    },"AL":
    {
      "codigo": "AL",
      "nombre": "Ministerio del Trabajo y Previsión Social",
      "mision": "Estudiar, elaborar y proponer políticas, planes, programas y normas orientados a la construcción de un sistema de relaciones laborales que privilegien la cooperación entre trabajadores y empresarios y sus respectivas organizaciones, así como la adecuada canalización de los conflictos, conduciendo los esfuerzos públicos hacia ese objetivo y articulándolos con los sectores sociales cuando corresponda.",
      "sigla": "",
      "created_at": "null",
      "updated_at": "null"
    },"AM":
    {
      "codigo": "AM",
      "nombre": "Ministerio de Obras Públicas",
      "mision": "Proveer al país de servicios de infraestructura para la conectividad, la protección del territorio y las personas, la edificación pública, y el aprovechamiento óptimo de los recursos hídricos, asegurando el cumplimiento de los estándares de servicio y la calidad de las obras, regulando el mercado asociado a los recursos hídricos y el cuidado del medio ambiente, para contribuir al desarrollo sustentable y competitividad del país, promoviendo la equidad, calidad de vida e igualdad de oportunidades de las personas.",
      "sigla": "",
      "created_at": "null",
      "updated_at": "null"
    },"AN":
    {
      "codigo": "AN",
      "nombre": "Ministerio de Transportes y Telecomunicaciones",
      "mision": "Su misión es proponer las políticas nacionales en materias de transportes y telecomunicaciones, de acuerdo a las directrices del Gobierno y ejercer la dirección y control de su puesta en práctica; supervisar las empresas públicas y privadas que operen medios de transportes y comunicaciones en el país.",
      "sigla": "",
      "created_at": "null",
      "updated_at": "null"
    },"AO":
    {
      "codigo": "AO",
      "nombre": "Ministerio de Salud",
      "mision": "La misión institucional que el Ministerio de Salud se ha dado para este período, busca contribuir a elevar el nivel de salud de la población; desarrollar armónicamente los sistemas de salud, centrados en las personas; fortalecer el control de los factores que puedan afectar la salud y reforzar la gestión de la red nacional de atención. Todo ello para acoger oportunamente las necesidades de las personas, familias y comunidades, con la obligación de rendir cuentas a la ciudadanía y promover la participación de las mismas en el ejercicio de sus derechos y sus deberes.",
      "sigla": "MINSAL",
      "created_at": "null",
      "updated_at": "null"
    },"AP":
    {
      "codigo": "AP",
      "nombre": "Ministerio de Vivienda y Urbanismo",
      "mision": "Su misión es mejorar la calidad de vida de chilenos y chilenas, posibilitando el acceso a viviendas dignas, barrios equipados y ciudades integradas.",
      "sigla": "MINVU",
      "created_at": "null",
      "updated_at": "null"
    },"AQ":
    {
      "codigo": "AQ",
      "nombre": "Ministerio de Bienes Nacionales",
      "mision": "Reconocer, administrar y gestionar el patrimonio fiscal; mantener actualizado el catastro gráfico de la propiedad fiscal; elaborar, en coordinación con las demás entidades del Estado, las políticas destinadas al aprovechamiento armónico del territorio para ponerlo al servicio del desarrollo económico, social y cultural del país, con una mirada integral y en forma sustentable, incluidas las áreas de escasa densidad poblacional; coordinar a las instituciones del Estado en materia de información territorial a través del Sistema Nacional de Información Territorial (SNIT); y regularizar la pequeña propiedad raíz particular, favoreciendo la igualdad de oportunidades entre mujeres y hombres, entregando servicios eficientes, transparentes, ágiles, oportunos y en continuo mejoramiento, especialmente a las personas de mayor vulnerabilidad.",
      "sigla": "",
      "created_at": "null",
      "updated_at": "null"
    },"AR":
    {
      "codigo": "AR",
      "nombre": "Ministerio de Agricultura",
      "mision": "Su misión es promover y aprovechar al máximo las ventajas comparativas que tiene la actividad silvoagropecuaria nacional y las oportunidades que la apertura y la globalización de la economía le abren al sector agropecuario. El diseño y puesta en operación de una Política de Estado en materia Silvoagropecuaria, que contenga programas de fomento productivo que favorezcan la modernización y adaptación de los rubros y segmentos de agricultores, especialmente pequeños y medianos, para los cuales este proceso de adaptación supone mayores desafíos y dificultades.",
      "sigla": "",
      "created_at": "null",
      "updated_at": "null"
    }, "AS":
    {
      "codigo": "AS",
      "nombre": "Ministerio de Minería",
      "mision": "Su misión es generar, fomentar, difundir y evaluar las normas y políticas que optimicen el desarrollo minero sustentable del país, maximicen su aporte al desarrollo económico social y consoliden su liderazgo internacional.",
      "sigla": "",
      "created_at": "null",
      "updated_at": "null"
    },"AT":
    {
      "codigo": "AT",
      "nombre": "Servicio Nacional de la Mujer",
      "mision": "Su misión es diseñar, proponer y coordinar políticas, planes, medidas y reformas legales conducentes a la igualdad de derechos y oportunidades entre hombres y mujeres; y a disminuir prácticas discriminatorias en el proceso de desarrollo político, social, económico y cultural del país.",
      "sigla": "SERNAM",
      "created_at": "null",
      "updated_at": "null"
    }, "AU":
    {
      "codigo": "AU",
      "nombre": "Ministerio de Energía",
      "mision": "El objetivo general del Ministerio de Energía es elaborar y coordinar los planes, políticas y normas para el buen funcionamiento y desarrollo del sector, velar por su cumplimiento y asesorar al Gobierno en todas aquellas materias relacionadas con la energía.\r\n\r\nEl sector energía comprende todas las actividades de estudio, exploración, explotación, generación, transmisión, transporte, almacenamiento, distribución, consumo, uso eficiente, importación y exportación, y cualquiera otra que concierna a la electricidad, carbón, gas, petróleo y derivados, energía nuclear, geotérmica y solar, y demás fuentes energéticas.",
      "sigla": "",
      "created_at": "null",
      "updated_at": "null"
    },"AV":
    {
      "codigo": "AV",
      "nombre": "Consejo Nacional de la Cultura y las Artes",
      "mision": "El Consejo Nacional de la Cultura y las Artes es el órgano del Estado encargado de implementar las políticas públicas para el desarrollo cultural. Nuestra misión es promover un desarrollo cultural armónico, pluralista y equitativo entre los habitantes del país, a través del fomento y difusión de la creación artística nacional; así como de la preservación, promoción y difusión del patrimonio cultural chileno, adoptando iniciativas públicas que estimulen una participación activa de la ciudadanía en el logro de tales fines.",
      "sigla": "",
      "created_at": "null",
      "updated_at": "null"
    },"AW":
    {
      "codigo": "AW",
      "nombre": "Ministerio del Medio Ambiente",
      "mision": "El Ministerio del Medio Ambiente de Chile es el órgano del Estado encargado de colaborar con el Presidente de la República en el diseño y aplicación de políticas, planes y programas en materia ambiental, así como en la protección y conservación de la diversidad biológica y de los recursos naturales renovables e hídricos, promoviendo el desarrollo sustentable, la integridad de la política ambiental y su regulación normativa.",
      "sigla": "",
      "created_at": "null",
      "updated_at": "null"
    },"AX":
    {
      "codigo": "AX",
      "nombre": "Consejo de Defensa del Estado",
      "mision": "",
      "sigla": "",
      "created_at": "null",
      "updated_at": "null"
    },"FU":
    {
      "codigo": "FU",
      "nombre": "Fundaciones",
      "mision": "",
      "sigla": "",
      "created_at": "null",
      "updated_at": "null"
    }, "MU":
    {
      "codigo": "MU",
      "nombre": "Municipios",
      "mision": "",
      "sigla": "",
      "created_at": "null",
      "updated_at": "null"
    }
  
}