from chalice import Chalice
from chalice import BadRequestError
import json
from chalicelib.comunas import list_comunas,comunas, comunas_provincia
from chalicelib.regiones import list_region
from chalicelib.provincias import list_provincias, list_endpoint, region_provincia
from chalicelib.entidades import list_entidades
from chalicelib.instituciones import lista_instituciones


app = Chalice(app_name='region-comuna')
#app.debug = True
                
def get_comunas():
    global list_comunas
    if list_comunas is None:
        list_comunas = comunas.list_comunas()
    return list_comunas

def get_listcomunas():
    global comunas
    if comunas is None:
        comunas = comunas.comunas()
    return comunas    

def get_region():
    global list_region
    if list_region is None:
        list_region = regiones.list_region()
    return list_region    


def get_provincias():
    global list_provincias
    if list_provincias is None:
        list_provincias = provincias.list_provincias()
    return list_provincias   

def get_provincias_comunas():
    global comunas_provincia
    if comunas_provincia is None:
        comunas_provincia = comunas.comunas_provincia()
    return comunas_provincia   

def get_region_provincias():
    global region_provincia
    if region_provincia is None:
        region_provincia = provincias.region_provincia()
    return region_provincia      

def get_instituciones():
    global lista_instituciones
    if lista_instituciones is None:
        lista_instituciones = instituciones.lista_instituciones()
    return lista_instituciones

 
@app.route('/')
def index():
    return {'Bienvenid@s a API SIMPLE': list_endpoint}    

@app.route('/regiones', cors=True, methods=['GET'])
def list_regiones():
    return  sorted(list_region.items())
   

@app.route('/regiones/{codigo}', cors=True, methods=['GET'])
def region(codigo):
    try:
        return list_region[codigo]
    except KeyError:
        raise BadRequestError("El código de la región ingresada '%s' es desconocido, las opciones de búsqueda de código de región son: %s" % (
            codigo, ', '.join(list_region.keys())))    

#Para Simple
@app.route('/regiones/{codigo}/comunas', cors=True, methods=['GET'])
def region_comunas(codigo):
    try:
        return list_comunas[codigo]
    except KeyError:
        raise BadRequestError("El código de la comuna para la región ingresada '%s' es desconocido, las opciones de búsqueda de código de comuna para la región son: %s" % (
            codigo, ', '.join(list_comunas.keys())))    

@app.route('/regiones/{codigo}/provincias', cors=True, methods=['GET'])
def region_provincias(codigo):
    try:
        return region_provincia[codigo]
    except KeyError:
        raise BadRequestError("El código de la provincia para la región ingresada '%s' es desconocido, las opciones de búsqueda de código de provincia para la región son: %s" % (
            codigo, ', '.join(region_provincia.keys())))               


@app.route('/comunas', cors=True, methods=['GET'])
def lista_comunas():
    return  list_comunas

@app.route('/comunas/{codigo}', cors=True, methods=['GET'])
def comuna(codigo):
    try:
        return comunas[codigo]    
    except KeyError:
        raise BadRequestError("El código de la comuna ingresada '%s' es desconocido, las opciones de búsqueda de código de comunas son: %s" % (
            codigo, ', '.join(comunas.keys())))  

@app.route('/provincias', cors=True, methods=['GET'])
def lista_provincias():
    return  list_provincias

@app.route('/provincias/{codigo}', cors=True, methods=['GET'])
def provincia(codigo):
    try:
        return list_provincias[codigo]
    except KeyError:
        raise BadRequestError("El código de la provincia ingresada '%s' es desconocido, las opciones de búsqueda de código de provincias son: %s" % (
            codigo, ', '.join(list_provincias.keys())))          

@app.route('/provincias/{codigo}/comunas', cors=True, methods=['GET'])
def provincia_comuna(codigo):
    try:
        return comunas_provincia[codigo]
    except KeyError:
        raise BadRequestError("El código de la comuna ingresada '%s' es desconocido para la provincia, las opciones de búsqueda de código de provincias son: %s" % (
            codigo, ', '.join(comunas_provincia.keys())))  

@app.route('/entidades', cors=True, methods=['GET'])   
def lista_entidades():
    return list_entidades 

@app.route('/entidades/{codigo}',cors=True, methods=['GET'])
def entidad(codigo):
    try:
        return list_entidades[codigo]
    except KeyError:
        raise BadRequestError("El código de la entidad ingresada '%s' es desconocido, las opciones de búsqueda de código de entidades son: %s" % (
            codigo, ', '.join(list_entidades.keys())))  

@app.route('/instituciones',cors=True, methods=['GET'])
def list_instituciones():
    return lista_instituciones 

@app.route('/instituciones/{codigo}', cors=True, methods=['GET'])   
def institucion(codigo):
    try:
        return lista_instituciones[codigo]
    except KeyError:
        raise BadRequestError("El código de la institucion ingresada '%s' es desconocido, las opciones de búsqueda de código de entidades son: %s" % (
            codigo, ', '.join(lista_instituciones.keys())))   

"""Endpoint B para Simple"""
@app.route('/entidades/{codigo}/instituciones', cors=True, methods=['GET'])   
def entidad_institucion(codigo):
    try:
        return lista_instituciones[codigo]
    except KeyError:
        raise BadRequestError("El código de la institucion ingresada '%s' es desconocido, las opciones de búsqueda de código de entidades son: %s" % (
            codigo, ', '.join(lista_instituciones.keys())))   
