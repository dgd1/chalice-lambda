# API REST 

Proyecto que contiene los siguientes endpoint: Región, comunas, entidades e intituciones. Creado con el microframework de Chalice con python 3.7.

## Estructura Proyecto 🚀

```

├── app.py
├── chalicelib
│   ├── __init__.py
│   └── comunas.py
│   └── entidades.py
│   └── instituciones.py
│   └── provincias.py
│   └── regiones.py 
└── requirements.txt

```

Mira **Deployment** para conocer como desplegar el proyecto.


### Pre-requisitos 📋

1. Para utilizar el proyecto usted previamente debe tener las credenciales de AWS configuradas en el archivo .aws de su computadora, si no las tiene configuradas puede seguir las siguientes instrucciones.

```
$ mkdir ~/.aws
$ cat >> ~/.aws/config
[default]
aws_access_key_id=YOUR_ACCESS_KEY_HERE
aws_secret_access_key=YOUR_SECRET_ACCESS_KEY
region=YOUR_REGION (us-west-2, us-west-1, etc)

```
2. Crear entorno virtual.

```

$ python3 --version
Python 3.7.3
$ python3 -m venv venv37
$ . venv37/bin/activate

```

3.  Instalar Chalice

```
python3 -m pip install chalice

```


## Deploy Proyecto 📦

Para desplegar el proyecto tienes las siguientes opciones:

```
chalice deploy --stage dev  -->Para desplegar la lambda a un ambiente de desarrollo en AWS.
chalice deploy --stage prod -->Para pasar la lambda a producción en AWS.

```

## Autor ✒️

 **EQUIPO SIMPLE/División Gobierno Digital** 


